﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using D08Are.Models;

namespace D08Are.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }

            
            var loc6 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var loc1 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            var loc2 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var loc3 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            var loc4 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            var loc5 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };

            context.Locations.AddRange(loc1, loc2, loc3, loc4, loc5, loc6);
             context.SaveChanges();

            context.Departments.AddRange(
            new Department() { DeptName = "ACS", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc1.LocationId },
            new Department() { DeptName = "AMS", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc2.LocationId },
            new Department() { DeptName = "AWS", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc3.LocationId},
            new Department() { DeptName = "AVF", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc4.LocationId },
            new Department() { DeptName = "AMH", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc5.LocationId },
            new Department() { DeptName = "AYF", DeptAddress = "COLDENHALL", contact = "66054212", LocationId = loc6.LocationId }

             );
            context.SaveChanges();
        }
    }
}

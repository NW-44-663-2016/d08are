﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace D08Are.Models
{
    //Department Model which contains details of Department Entity
    public class Department
    {
        [ScaffoldColumn(false)]
        [Key]
        public int DeptId { get; set; }
        [Required]
        [Display(Name = "Dept Name")]
        public string DeptName { get; set; }
        [Display(Name = "Dept Address")]
        public string DeptAddress { get; set; }
        public string contact { get; set; }
        [ScaffoldColumn(true)]
        public int? LocationId { get; set; }
        public virtual Location Location { get; set; }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Are.Models
{
    public class ApplicationDbContext : DbContext
    {
       public DbSet<Department> Departments { get; set;}
       public DbSet<Location> Locations { get; set; }
    }
}
